PUBNUB = Pubnub.new(
    :publish_key   =>  APP_CONFIG['pubnub_pub_key'], # publish_key only required if publishing.
    :subscribe_key => APP_CONFIG['pubnub_sub_key'], # required
    :secret_key    => APP_CONFIG['pubnub_secret_key'],    # optional, if used, message signing is enabled
    :cipher_key    => APP_CONFIG['pubnub_cipher_key'],    # optional, if used, encryption is enabled
    :ssl           => APP_CONFIG['pubnub_ssl']     # true or default is false
)
