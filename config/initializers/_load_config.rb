APP_CONFIG = HashWithIndifferentAccess.new YAML.load_file("#{Rails.root}/config/config.yml")[Rails.env]

#LOAD ENV FROM FILE ON DEV
env_file = File.join(Rails.root.join 'config', 'config.yml')
YAML.load(File.open(env_file)).each do |key, value|
  ENV[key.to_s] = value if value.is_a? String
  value.each do |key, value|
    ENV[key.to_s] = value
  end if key==Rails.env
end if File.exists?(env_file)