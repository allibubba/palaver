Palaver::Application.routes.draw do
  root :to => 'chatrooms#list'
  get '/chatrooms/new' => 'chatrooms#new', :as => :new_chatroom
  get '/chatrooms/:filter' => "chatrooms#list", :as => :chatroom_filter

  get '/twitter' => "main#twitter"
  get "/chatroom/:id/join" => "chatrooms#join", :as => :join_chatroom
  match "/auth/:provider" => 'session#create', :as => :social_login
  match "/auth/:provider/callback" => 'session#create'
  match "/auth/failure" => 'session#failure'
  match "/auth/identity" => 'session#identity', :as => :login
  match "/auth/identity/register" => 'session#register', :as => :register
  match "/logout" => 'session#destroy', :as => :logout
  match "/connection/remove/:id" => 'user#remove_connection', :as => :remove_connection


  get "/:id" => "chatrooms#show", :as => :chatroom
  put "/:id" => "chatrooms#update"
  delete "/:id" => "chatrooms#destroy"

  put "post/reject/:id" => "posts#reject", :as => :reject_post
  put "post/:id/:profile_id" => "posts#assign_to_profile", :as => :post_assign

  delete "/account/:account_id/member/:member_id" => "accounts#remove_member", :as => :remove_member

  resources :chatrooms
  resources :posts
  resources :accounts
  resources :profiles
  resources :user
  resources :session

end
