require "bundler/setup"

require "bundler/capistrano"
require 'hipchat/capistrano'

system('eval `ssh-agent`')

set :user, "roundhouse"
set :application, "Palaver"
set(:deploy_to) { "/home/#{user}/apps/#{application}" }
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, "git"
set :repository, "git@github.com:roundhouse/#{application}.git"
set :origin, "github"
set :branch, "master"

default_run_options[:pty] = true
ssh_options[:forward_agent] = true


# hipchat settings
set :hipchat_room_name, application
set :hipchat_token, "8cc50952b181050dc3c210973b48b2"
set :hipchat_announce, false # notify users
set :hipchat_failed_color, 'red' #cancelled deployment message color

set :stages, %w(production staging)
set :default_stage, "staging"
# default environment variables object
set :default_environment, {}
require 'capistrano/ext/multistage'

# loads you always want
load "config/recipes/base"
load "config/recipes/nginx"
load "config/recipes/unicorn"
load "config/recipes/nodejs"
load "config/recipes/rbenv"
load "config/recipes/check"
load "config/recipes/hipchat"
load "config/recipes/monit"
load "config/recipes/imagemagick"

before "deploy:assets:precompile", "env:symlink_env" # update config symlink after asset compile
after "deploy", "deploy:cleanup" # keep only the last 5 releases
after "deploy:update_code", "bundle:install"


# loads you should be changing or adding
load "config/recipes/mysql"
# load "config/recipes/postgresql"
# load "config/recipes/mongo"
# load "config/recipes/redis"

# global configurations you can change

# require 'sidekiq/capistrano'

# set :sidekiq_role, :app
# set :sidekiq_pid, "#{current_path}/tmp/pids/sidekiq.pid"
