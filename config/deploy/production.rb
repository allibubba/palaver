server "chater-palaver.cloudapp.net", :web, :app, :db, primary: true

set :domain, "chater-palaver.cloudapp.net"

set :rails_env, "production"

# config sidekiq
# set :sidekiq_timeout, 10
# set :sidekiq_processes, 1

# hipchat settings
set :hipchat_env, "production"
set :hipchat_color, 'purple' #finished deployment message color