server "chater-palaver.cloudapp.net", :web, :app, :db, primary: true

set :domain, "chater-palaver.cloudapp.net"

set :rails_env, "staging"


# environment specific configurations

#config sidekiq
# set :sidekiq_timeout, 10
# set :sidekiq_processes, 1

# hipchat settings
set :hipchat_env, "staging"
set :hipchat_color, 'green' #finished deployment message color