class IdentityConnection < Connection
  include OmniAuth::Identity::Model
  include OmniAuth::Identity::SecurePassword


  attr_accessible  :password, :password_confirmation

  self.abstract_class = true
  has_secure_password
  auth_key('nickname')
  def self.auth_key=(key)
    super
    validates_uniqueness_of key, :case_sensitive => false
  end

  def self.locate(search_hash)
    where(search_hash).first
  end
end
