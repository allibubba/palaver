class Theme < ActiveRecord::Base
  attr_accessible :account_id, :chatroom_id

  belongs_to :chatroom
  belongs_to :account

  validates_presence_of :account_id, :chatroom_id
end
