class Response < ActiveRecord::Base
  attr_accessible :post_id, :response_id
  belongs_to :post
  belongs_to :state, :class_name => "Post"
end