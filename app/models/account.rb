class Account < ActiveRecord::Base
  extend FriendlyId
  include Status

  friendly_id :name, use: :slugged

  attr_accessible :fbid, :fbpage, :name, :slug, :user_id, :status

  has_many :chatrooms
  has_and_belongs_to_many :members, :class_name => "User"
  belongs_to :owner, :class_name => "User", :foreign_key => "user_id"

  validates_uniqueness_of :slug
  validates_presence_of :name

end
