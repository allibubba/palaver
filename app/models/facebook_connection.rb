class FacebookClient < Koala::Facebook::API #https://github.com/arsduo/koala/wiki/Graph-API
  def initialize(options)
    config = {}
    config[:access_token] = options[:user_token] if options[:user_token]
    super(config[:access_token])
  end
end

class FacebookRealTimeClient < Koala::Facebook::RealtimeUpdates #https://github.com/arsduo/koala/wiki/Realtime-Updates
  def initialize(options)
    config = {}
    config[:app_id] = options[:app_id] if options[:app_id]
    config[:secret] = options[:app_secret] if options[:app_secret]
    super(config)
  end
end

class FacebookConnection < Connection
  attr_accessor :real_time_client

  def after_initialize_callback
    @client = FacebookClient.new(:app_id => APP_CONFIG['fb_app_id'], :app_secret => APP_CONFIG['fb_app_secret'], :user_token => self.token)
  end

  def upload_photo
    @client.upload_photo
  end

  def real_time
    @real_time_client = @real_time_client || FacebookRealTimeClient.new(:app_id => APP_CONFIG['fb_app_id'], :app_secret => APP_CONFIG['fb_app_secret'])
  end

  def me
    @client.get_object('/me') if authenticated?
  end

  def likes
    @client.get_connections(self.uid,'likes') if authenticated?
  end


  def share
    raise "SHARING FOR FACEBOOK"
  end

end
