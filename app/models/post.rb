class Post < ActiveRecord::Base
  include Status
  default_scope open.where("status != 'destroyed'")
  scope :open, where(status: 'open')
  scope :rejected, where(status: 'rejected')

  attr_accessible :chatroom_id, :message, :slug, :type, :post_id, :parent_id

  belongs_to :profile
  belongs_to :chatroom, :class_name => "Chatroom", :foreign_key => "chatroom_id"
  belongs_to :parent, :class_name => 'Post'

  has_many :replies, :class_name => 'Post', :foreign_key => 'parent_id'
  has_and_belongs_to_many :celebrities

  validates_presence_of :profile_id, :chatroom_id, :message

  def create
    if self.profile && !self.profile.banned
      self.chatroom_id = self.profile.chatroom_id
      super
    else
      raise PostException.new(:message => 'No profile for the message.') unless self.profile
      raise PostException.new(:message => 'User is banned from chatroom.') if self.profile.banned
    end
  end

  def author
    self.profile.user
  end

  def type
    self.profile.type
  end

  def has_replies?
    if self.replies.size > 0
      true
    else
      false
    end
  end

  def self.statuses
    %W(open pending rejected)
  end

  def reject
    self.update_attributes(status: 'rejected')
  end
end