module Status
  extend ActiveSupport::Concern
  module ClassMethods
    def open
      where(:status => 'open')
    end

    def closed
      where(:status => 'closed')
    end

    def deleted
      where(status: 'destroyed')
    end

    def statuses
      %W(open closed)
    end
  end

  module InstanceMethods
    def open
      self.update_attributes status: 'open'
    end

    def open?
      true if self.status == 'open'
    end

    def closed?
      true if self.status == 'closed'
    end

    def close
      self.update_attributes status: 'closed'
    end

    def soft_destroy
      self.dependant_state_update('destroyed')
    end

    def soft_destroy_restore(state = 'closed')
      self.dependant_state_update(state)
    end

    def state
      self.status
    end

    def dependant_state_update(state)
      self.update_attributes status: state
      self.reflections.each do |rel|
        if rel.last.klass.method_defined? :dependant_state_update
          if rel.last.options[:dependent] == :destroy
            items = [] + self.send(rel.last.name)
            items.each do |item|
              item.dependant_state_update state
            end
          end
        end
      end
    end


  end

  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
    receiver.attr_accessible :status
    # receiver.has_one :status, :as => :statusable # add relationship to model when the module is included...
  end
end