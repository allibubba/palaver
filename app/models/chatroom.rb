class Chatroom < ActiveRecord::Base
  extend FriendlyId
  include Status
  default_scope where("status != 'destroyed'")
  scope :closed, where(:status => 'closed')
  
  friendly_id :name, use: :history

  include ActiveSupport

  attr_accessible :name, :banned_users, :account_id, :hide_replies, :fbappid, :fbappnamespace, :fpappsecret, :moderation, :slug, :theme_id, :timezone
  attr_accessor :date_start, :date_end

  belongs_to :account
  has_one :owner, :through => :account

  has_many :profiles, :dependent => :destroy
  has_many :users, :through => :profiles
  has_many :celebrities
  has_many :moderators
  has_many :viewers
  has_many :posts

  validates_uniqueness_of :slug

  def banned_profiles
    self.profiles.banned
  end

  def is_profile_banned?(profile)
    self.banned_profiles.include?(profile)
  end

  def is_user_banned?(user)
    if user.class == User
      profiles = user.profiles
      intersection = self.banned_profiles & profiles
      return true if intersection.size > 0
    elsif user.class == Profile
      self.is_profile_banned?(user)
    end
  end

  def messages
    self.posts.where(:parent_id => nil).order('updated_at ASC').open
  end
  
  def join(user)
    
  end
  
  def add_viewer(user)
    unless self.has_user?(user)
      usernames = self.profiles.collect {|u| u.display_name}
      raise ProfileException.new(message: "Display name is already taken") if usernames.include? user.username
      
      viewer = Viewer.create(chatroom: self, user_id: user.id, status: 'open', :display_name => user.username, :gravatar => Digest::MD5.hexdigest(user.email.presence || ""))
      self.viewers << viewer
      
      # begin
      # rescue => e
      # end
    end
  end
  
  def has_user?(user)
    self.users.include?(user)
  end
  
  #  ***********************************************
  #   MODERATOR MANAGEMENT      --- 01-08-2013
  # ***********************************************
  def add_moderator(user)
    moderator = Moderator.new(:chatroom => self, :user_id => user.id, :display_name => user.username)
    moderator.open
  end

  def add_moderators(moderators)
    moderators.each do |moderator|
      add_moderator(moderator)
    end
  end

  def update_moderators(users)
    new_profiles = []
    existing_profiles = self.moderators
    users = users + [self.owner]
    users.each do |user|
      profile = user.get_profile_for_chatroom self
      if profile
        new_profiles << profile
      else
        new_profiles <<  Moderator.new(:chatroom => self, :user_id => user.id, :display_name => user.username)
      end
    end
    update_profiles(new_profiles,existing_profiles,"Moderator")
  end

  #  ***********************************************
  #   CELEBRITY MANAGEMENT      --- 01-08-2013
  # ***********************************************
  def add_celebrity(user)
    self.celebrities.create(:chatroom => self, :user_id => user.id, :display_name => user.username, :status => 'open')
  end

  def update_celebrities(users)
    new_profiles = []
    existing_profiles = self.celebrities
    users.each do |user|
      profile = user.get_profile_for_chatroom self
      if profile
        new_profiles << profile
      else
        new_profiles <<  Celebrity.new(:chatroom => self, :user_id => user.id, :display_name => user.username)
      end
    end
    update_profiles(new_profiles,existing_profiles,"Celebrity")
  end

  #  ***********************************************
  #   CHATROOM PROFILE UPDATES      --- 01-08-2013
  # ***********************************************
  def update_profiles(new_profiles,existing_profiles,type)
    profiles_to_add = new_profiles - existing_profiles
    profiles_to_remove = existing_profiles - new_profiles
    profiles_to_add.each do |prof|
      prof.type = type
      prof.open
    end
    profiles_to_remove.each do |prof|
      prof.destroy
    end
  end
end
