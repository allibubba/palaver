class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: :slugged

  attr_accessible :email, :password, :username, :slug, :type
  attr_accessor :name

  attr_encrypted :email, :key => APP_CONFIG['encryption_key'], :encode => true
  attr_encrypted :password, :key => APP_CONFIG['encryption_key'], :encode => true

  has_many :profiles
  has_many :posts, :through => :profiles
  has_one :account

  has_many :connections, :dependent => :destroy
  has_and_belongs_to_many :memberships, :class_name => "Account"
  validates_presence_of :username
  validates_uniqueness_of :slug, :username


  # scope :banned, where(:account_id => nil)

  def name
    self.username
  end

  def get_connection(connection)
    self.connections.find {|c| c.type == "#{connection.to_s.gsub('Connection','')}Connection".classify}
  end

  def has_connection?(connection)
    true if get_connection(connection)
  end

  def add_connection(connection)
    self.connections << connection
  end

  def get_profile_for_chatroom(chatroom)
    self.profiles.unscoped.find_by_chatroom_id_and_user_id(chatroom.id,self.id)
  end


  def self.find_or_create_from_connection(connection)
    unless connection.user
      u = User.create(:username => connection.nickname)
      u.connections << connection
    end
    connection.user
  end

  def self.find_or_create_from_omniauth(auth_hash,password=nil,password_confirmation=nil)
    c = Connection.find_or_create_from_omniauth(auth_hash,password,password_confirmation)
    unless c.user
      u = User.create!(:username => c.nickname)
      u.connections << c
    end
    c.user
  end
  # END OMNIAUTH CONNECTIONS

  private

  def encrypted_email
    read_attribute(:encrypted_email)
  end




end
