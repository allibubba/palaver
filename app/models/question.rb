class Question < Post
  has_many :responses, :class_name => "Response", :foreign_key => "response_id"
end
