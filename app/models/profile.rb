class Profile < ActiveRecord::Base
  include Status
  attr_accessible :chatroom, :description, :display_name, :image, :user_id, :gravatar
  mount_uploader :image, ProfileImageUploader

  default_scope where("status != 'destroyed'")
  scope :banned, where(:banned => true)

  belongs_to :chatroom, :class_name => "Chatroom", :foreign_key => "chatroom_id"
  belongs_to :user
  # belongs_to :celebrity, :class_name => "Celebrity", :foreign_key => "user_id"
  # belongs_to :viewer, :class_name => "Viewer", :foreign_key => "user_id"
  # belongs_to :moderator, :class_name => "Moderator", :foreign_key => "user_id"
  has_many :posts, :dependent => :destroy

  validates_presence_of :user_id, :chatroom_id, :display_name, :type
  validates_uniqueness_of :user_id, :scope => :chatroom_id
  validates_uniqueness_of :display_name, :scope => :chatroom_id

  def thumbnail
    self.image.thumb || "http://www.gravatar.com/avatar/#{self.gravatar}?d=identicon"
  end

  def is_a?(profile_type)
    true if self.type == profile_type
  end

end
