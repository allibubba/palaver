class PostsController < ApplicationController
  before_filter :moderator_check, :only => [:reject, :assign_to_profile]

  def create
    @profile = Profile.find_by_user_id_and_chatroom_id(@current_user.id, params[:post][:chatroom_id])
    post = Post.create(params[:post])

    if (post.parent && @profile.type == "Celebrity") && @profile.questions.include?(post.parent)
      @profile.questions.delete post.parent
      PUBNUB.publish(
        :channel  => "chatroom_#{params[:post][:chatroom_id]}_celebrity_#{@profile.id}_answered",
        :message  => {post: post, type: @profile.type.downcase, profile: @profile},
        :callback => lambda { |message| 
          pp "/* Message Posted ***********************************************"
          pp message
          pp "*********************************************** */"
        }
      )      
      # PrivatePub.publish_to("/messages/new/#{params[:post][:chatroom_id]}/celebrity/#{@profile.id}/answered", post: post, type: @profile.type.downcase, profile: @profile)
    end
    @profile.posts << post

    @push_channel = "message_added_#{params[:post][:chatroom_id]}"
    PUBNUB.publish(
      :channel  => @push_channel,
      :message  => {post: post, type: @profile.type.downcase, profile: @profile},
      :callback => lambda { |message| 
        pp "/* Message Posted ***********************************************"
        pp message
        pp "*********************************************** */"
      }
    )
    # PrivatePub.publish_to("/messages/new/#{params[:post][:chatroom_id]}", post: post, type: @profile.type.downcase, profile: @profile)
    respond_to do |format|
      format.html {redirect_to :back}
      format.json {render :json => post.to_json}
      format.js {render :json => post.to_json}
    end
  end

  def assign_to_profile
    if @moderator
      @profile = Profile.find(params[:profile_id])
      
      if @profile.type == "Celebrity"
        unless @profile.questions.include? @post
          @profile.questions << @post
          PUBNUB.publish(
            :channel  => "chatroom_#{@profile.chatroom_id}_celebrity_#{@profile.id}",
            :message  => {post: @post, type: @post.profile.type.downcase, profile: @post.profile},
            :callback => lambda { |message| 
              pp "/* Message Posted ***********************************************"
              pp message
              pp "*********************************************** */"
            }
          )
          # PrivatePub.publish_to("/messages/new/#{@profile.chatroom_id}/celebrity/#{@profile.id}", post: @post, type: @post.profile.type.downcase, profile: @post.profile)
        end
      end

    end
    redirect_to :back
  end

  def reject
    if @moderator
      @post.reject
      PUBNUB.publish(
        :channel  => "message_rejected_#{@post.chatroom_id}",
        :message  => {post: @post, type: @post.profile.type.downcase, profile: @post.profile},
        :callback => lambda { |message| 
          pp "/* Message Posted ***********************************************"
          pp message
          pp "*********************************************** */"
        }
      )
      
      # PrivatePub.publish_to("/messages/reject/#{@moderator.chatroom_id}", post: @post, type: @moderator.type, profile: @moderator)
    else
    end

    respond_to do |format|
      format.html {redirect_to :back}
      format.json {render :json => @post.to_json}
      format.js {render :json => @post.to_json}
    end

  end

  def moderator_check
    @moderator = false
    @post = Post.find(params[:id])
    if @current_user
      profile = Profile.find_by_user_id_and_chatroom_id(@current_user.id,@post.chatroom.id)
      @moderator = profile if profile.type = "Moderator"
    end
  end

end
