class ApplicationController < ActionController::Base
  protect_from_forgery

  # Add authentication to the stage and prod environments
  before_filter :authenticate
  before_filter :current_user, :except => [:destroy]

  def authenticate
    if Rails.env == 'staging' && !is_facebook?(request.remote_ip)
      authenticate_or_request_with_http_basic do |username, password|
        username == "katty" && password == "chathy"
      end
    end
  end

  def is_facebook?(_ip)
    require "ipaddr"
    low  = IPAddr.new("66.220.144.0").to_i
    high = IPAddr.new("66.220.159.255").to_i
    ip   = IPAddr.new(_ip).to_i
    (low..high)===ip
  end

  private

    def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end

    def set_current_user (user)
      pp "/* ***********************************************"
        pp "SET CURRENT USER"
        pp user
      pp "*********************************************** */"

      if user
        @current_user = user
        session[:user_id] = @current_user.id
      else
        raise UserLoginError
      end
    end

    helper_method :current_user
end
