class ChatroomsController < ApplicationController
  def list
    @filter = params[:filter] || Chatroom.statuses.first
    @chatrooms = Chatroom.send(@filter)
  end


  def show

    @chatroom = Chatroom.find(params[:id])
    @current_profile = @current_user.get_profile_for_chatroom(@chatroom) if @current_user
    session[:return_url] = join_chatroom_path(@chatroom) unless @current_user

    @show_replies = true
    if @chatroom.hide_replies
      @show_replies = false unless @current_profile
    end
    # Set up subscribe channels
    @reject_channel = "message_rejected_#{@chatroom.id}"
    @push_channel = "message_added_#{@chatroom.id}"
    @answered_channel = nil

    if @current_profile &&  @current_profile.is_a?("Celebrity")
      @posts = @current_profile.questions
      @push_channel = "chatroom_#{@chatroom.id}_celebrity_#{@current_profile.id}"
      @answered_channel = "chatroom_#{@chatroom.id}_celebrity_#{@current_profile.id}_answered"
    else
      @posts = @chatroom.messages
    end
    @in_chat = @chatroom.users.include?(@current_user)
  end

  def join
    @chatroom = Chatroom.find(params[:id])
    
    if !@current_user
      session[:return_url] = chatroom_path(@chatroom)
      redirect_to :login
    else
      @in_chat = @chatroom.has_user?(@current_user)
      unless @in_chat
        begin
          @chatroom.add_viewer(@current_user)
        rescue => error
          redirect_to :edit_profile, :notice => "Your username is already taken in that chat, please edit this profile!"
        end
      else
        redirect_to @chatroom
        
      end
    end
  end

  def edit
    @chatroom = Chatroom.find(params[:id])
    if @chatroom.account.owner != @current_user
      redirect_to chatroom_path(@chatroom)
    end
  end

  def update
    @chatroom = Chatroom.find(params[:id])
    s = @chatroom.update_attributes(params[:chatroom])
    celebs = params[:celebrities] ? User.find(params[:celebrities]) : []
    @chatroom.update_celebrities( celebs )
    mods = params[:moderators] ? User.find(params[:moderators]) : []
    @chatroom.update_moderators( mods )
    if s
      notice = "Chatroom Updated"
    else
      notice = "Chatroom was not updated."
    end
    redirect_to :back, :notice => notice
  end

  def create
    @chatroom = Chatroom.create(params[:chatroom])
    @chatroom.add_moderator(@current_user)
    celebs = params[:celebrities] ? User.find(params[:celebrities]) : []
    @chatroom.update_celebrities( celebs )
    mods = params[:moderators] ? User.find(params[:moderators]) : []
    @chatroom.update_moderators( mods )
    redirect_to @chatroom
  end

  def new
    if !@current_user
      session[:return_url] = new_chatroom_path
      redirect_to :login, :notice => "You must login and have an account first."
    elsif @current_user && !@current_user.account
      session[:return_url] = new_chatroom_path
      redirect_to edit_user_path(@current_user), :notice => "You must have an account before you create a chatroom"
    end
    @chatroom = Chatroom.new
  end

  def destroy
    c = Chatroom.find(params[:id])
    s = c.soft_destroy
    if s
      notice = "Chatroom has been removed."
    else
      notice = "Cannot delete chatroom."
    end
    redirect_to :back, :notice => notice
  end

  def format_post_for_mustache(post)
    {
      in_chat: @in_chat,
      message: post.as_json,
      profile: post.profile.as_json,
      type: post.profile.type.downcase
    }
  end
  helper_method :format_post_for_mustache

end
