class SessionController < ApplicationController

  def create
    pw = params[:password] || nil
    pwc = params[:password_confirmation] || nil
    current_connection = Connection.find_or_create_from_omniauth(auth_hash,pw,pwc)




    if params[:account_id] && @current_user
      if current_connection.user
        member = current_connection.user
      else
        member = User.find_or_create_from_omniauth(auth_hash,pw,pwc)
      end
      @current_user.account.members << member
      redirect_to :back, :notice => "User Added"
    else
      if @current_user #USER IS LOGGED IN
        unless (@current_user.has_connection(current_connection) and !current_connection.user)
          @current_user.add_connection(current_connection)
        else #LOGGED IN USER DOESN'T MATCH THE CONNECTING USER
          ref = User.reflections.each do |r|
            collection = current_connection.user.send(r.last.plural_name)
            collection.each do |c|
              c.user = @current_user
              c.save
            end
          end
        end
      else #USER IS NOT LOGGED IN
        if current_connection.user
          set_current_user(current_connection.user)
        else
          u = User.find_or_create_from_omniauth(auth_hash,pw,pwc)
          set_current_user(u)
        end
      end
      redirect_to session[:return_url] || :root
    end
  end

  def failure
    raise "AUTH FAILURE"
  end

  def destroy
    session[:user_id] = nil
    session[:return_url] = nil
    redirect_to :root
  end

  def show
  end

  def login
  end

  def register
  end

  protected
  def auth_hash
    request.env['omniauth.auth']
  end
end
