class ProfilesController < ApplicationController
  def index
  end

  def new
  end

  def create
  end

  def edit
    if @current_user
      
    else
      redirect_to :login
    end
  end

  def destroy
  end

  def update
    p = Profile.find(params[:id])
    p.update_attributes(params[:profile])
    redirect_to :back
  end
end
