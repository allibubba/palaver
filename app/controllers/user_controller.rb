class UserController < ApplicationController
  before_filter :check_profile_owner, :only => [:show,:edit, :update]

  def list
  end

  def show
    @account_members = @current_user.account.members if @current_user.account && @current_user.account.members
    render :edit if @user == @current_user
  end

  def new
  end

  def create
  end

   def edit
    redirect_to :profile unless @owner
   end

  def update
    @user.update_attributes(params[:user])
    redirect_to user_path(@user), :notice => "User has been updated!"
  end

   def delete
   end

   def check_profile_owner
     @user = User.find(params[:id])
     @owner = true unless @user != @current_user
   end
end
