class AccountsController < ApplicationController
  before_filter :check_current_user

  def index
  end

  def show
  end

  def new
    @account = @current_user.account || Account.new
  end

  def update
    @account = Account.find(params[:id])
    @account.update_attributes(params[:account])
    redirect_to :back, :notice => "Account has been updated!"
  end

  def remove_member
    @account = Account.find(params[:account_id])
    @member = User.find(params[:member_id])
    response = @account.members.delete @member

    respond_to do |format|
      format.html {redirect_to :back}
      format.json {render :json => response.to_json}
      format.js {render :json => response.to_json}
    end


  end


  def create
    Account.create!(params[:account])
    if session[:return_url]
      redirect_to session[:return_url], :notice => "Account has been created! Thank you."
    end
  end

  def destroy
  end

  def check_current_user
    redirect_to :root, notice: "You must be logged in to create an account" unless @current_user
  end
end
