#  ***********************************************
#  FOR EVENTS onConnect, onReconnect, onDisconnect The Channel Manager functions will fire first, then call the optional event on the individual NotificationManager       --- 03-26-2013
# *********************************************** 

class @NotificationManager
  constructor: (channel,options = {}) ->
    {@onConnect,@onDisconnect,@onReconnect,@onUpdate,@history_index,@history_qty} = options
    @channel = channel ? null
    @actions = {}
    @history_index = @history_index ? 1
    @history_qty = @history_qty ? 100
  messageReceived: (message) =>
    if message.action
      @actions[message.action](message.parameters) if @actions[message.action]
    else if @onUpdate
      @onUpdate(message)
  addAction: (name,action) =>
    @actions[name] = action
    @
  removeAction: (name) =>
    @actions[name] = null
    @


class @NotificationChannelManager
  constructor: (options = {}) ->
    {@onConnect,@onDisconnect,@onReconnect,@onUpdate, @subscribe_key,@publish_key} = options
    if typeof PUBNUB is "undefined"
      throw new Error("Pubnub is not defined - Make sure it is included.")
    else if !@publish_key && !@subscribe_key
        throw new Error("You must provide a publish key or a subscription key")

    @pusher = PUBNUB.init(
      publish_key   : @publish_key,
      subscribe_key : @subscribe_key,
      origin        : "pubsub.pubnub.com",
      windowing     : 1000
    )
    @notification_managers = []
  subscribe: (manager) =>
    @pusher.subscribe({
      channel  : manager.channel,
      connect  : @connected,
      disconnect: @disconnected,
      reconnect: @reconnected,
      restore: true,
      callback : @messageReceived
    })
  addNotificationManager: (notification_manager) =>
    @notification_managers.push(notification_manager)
    @subscribe(notification_manager)
  connected: (channel) =>
    @onConnect(arguments) if @onConnect
    for own manager, index in @notification_managers
      if manager.channel is channel
        manager.onConnect(channel) if manager.onConnect
  disconnected: (channel) =>
    @onDisconnect(arguments) if @onDisconnect
    for own manager, index in @notification_managers
      if manager.channel is channel
        manager.onDisconnect(channel) if manager.onDisconnect
  reconnected: (channel) =>
    @onReconnect(arguments) if @onReconnect
    for own manager, index in @notification_managers
      if manager.channel is channel
        manager.onReconnect(channel) if manager.onReconnect
  messageReceived: (message,envelope) =>
    # find message_controller with name == envelope
    for own manager, index in @notification_managers
      last = envelope[envelope.length - 1]
      @onUpdate(message,manager) if @onUpdate
      if manager.channel == last
        manager.messageReceived(message,envelope)
  fetchHistory: (manager) =>
    @pusher.history({
      count: manager.history_qty,
      channel: manager.channel
      callback: (messages) =>
        @historyReceived(messages,manager)
    })
  historyReceived: (messages,manager) =>
    for own message in messages[0]
      manager.messageReceived(message)
