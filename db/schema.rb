# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130201011300) do

  create_table "accounts", :force => true do |t|
    t.string   "slug"
    t.string   "name"
    t.string   "fbpage"
    t.string   "fbid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
    t.string   "status"
  end

  create_table "accounts_users", :id => false, :force => true do |t|
    t.integer "account_id"
    t.integer "user_id"
  end

  create_table "celebrities_posts", :id => false, :force => true do |t|
    t.integer "celebrity_id"
    t.integer "post_id"
  end

  create_table "chatrooms", :force => true do |t|
    t.integer  "account_id"
    t.integer  "theme_id"
    t.string   "slug"
    t.string   "fbappid"
    t.string   "fpappsecret"
    t.string   "fbappnamespace"
    t.datetime "date_start"
    t.datetime "date_end"
    t.string   "status"
    t.boolean  "moderation"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "timezone"
    t.string   "name"
    t.boolean  "hide_replies"
  end

  add_index "chatrooms", ["slug"], :name => "index_chatrooms_on_slug"

  create_table "connections", :force => true do |t|
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "uid"
    t.string   "provider"
    t.string   "type"
    t.string   "password_digest"
    t.string   "nickname"
    t.integer  "user_id"
    t.string   "token"
    t.string   "refresh_token"
    t.datetime "expires_at"
  end

  create_table "friendly_id_slugs", :force => true do |t|
    t.string   "slug",                         :null => false
    t.integer  "sluggable_id",                 :null => false
    t.string   "sluggable_type", :limit => 40
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type"], :name => "index_friendly_id_slugs_on_slug_and_sluggable_type", :unique => true
  add_index "friendly_id_slugs", ["sluggable_id"], :name => "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], :name => "index_friendly_id_slugs_on_sluggable_type"

  create_table "posts", :force => true do |t|
    t.string   "slug"
    t.text     "message"
    t.string   "type"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "chatroom_id"
    t.integer  "profile_id"
    t.integer  "parent_id"
    t.string   "status"
  end

  create_table "profiles", :force => true do |t|
    t.string   "image"
    t.integer  "user_id"
    t.text     "description"
    t.integer  "chatroom_id"
    t.string   "display_name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "type"
    t.boolean  "banned"
    t.string   "gravatar"
    t.string   "status"
  end

  add_index "profiles", ["user_id", "chatroom_id"], :name => "index_profiles_on_user_id_and_chatroom_id", :unique => true

  create_table "responses", :id => false, :force => true do |t|
    t.integer "post_id"
    t.integer "response_id"
  end

  create_table "themes", :force => true do |t|
    t.integer  "account_id"
    t.integer  "chatroom_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "encrypted_email"
    t.string   "encrypted_password"
    t.string   "slug"
    t.string   "type"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "username"
  end

end
