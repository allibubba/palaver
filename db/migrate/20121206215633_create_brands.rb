class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :slug
      t.string :name
      t.string :fbpage
      t.string :fbid

      t.timestamps
    end
  end
end
