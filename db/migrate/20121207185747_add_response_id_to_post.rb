class AddResponseIdToPost < ActiveRecord::Migration
  def self.up
    add_column :posts, :response_id, :integer
  end

  def self.down
    remove_column :posts, :response_id
  end
end