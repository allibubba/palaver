class AddStatusIdToAccount < ActiveRecord::Migration
  def self.up
    add_column :accounts, :status_id, :integer
  end

  def self.down
    remove_column :accounts, :status_id
  end
end