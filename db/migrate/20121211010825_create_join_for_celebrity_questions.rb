class CreateJoinForCelebrityQuestions < ActiveRecord::Migration
  def self.up
    create_table :celebrities_posts, :force => true, :id => false do |t|
      t.integer :celebrity_id
      t.integer :post_id
    end
  end

  def self.down
    drop_table :celebrities_posts
  end
end