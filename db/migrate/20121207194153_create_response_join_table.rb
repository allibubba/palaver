class CreateResponseJoinTable < ActiveRecord::Migration
  def self.up
    create_table :responses, :force => true, :id => false do |t|
      t.integer :post_id
      t.integer :response_id
    end
  end

  def self.down
    drop_table :responses
  end
end