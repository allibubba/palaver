class CreateChatrooms < ActiveRecord::Migration
  def change
    create_table :chatrooms do |t|
      t.integer :brand_id
      t.integer :theme_id
      t.string :slug
      t.string :fbappid
      t.string :fpappsecret
      t.string :fbappnamespace
      t.datetime :date_start
      t.datetime :date_end
      t.integer :status_id
      t.integer :banned_users
      t.boolean :moderation
      t.boolean :enable_frontend_moderation

      t.timestamps
    end
  end
end
