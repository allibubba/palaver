class AddTimezoneToChatroom < ActiveRecord::Migration
  def self.up
    add_column :chatrooms, :timezone, :string
  end

  def self.down
    remove_column :chatrooms, :timezone
  end
end