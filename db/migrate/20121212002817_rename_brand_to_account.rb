class RenameBrandToAccount < ActiveRecord::Migration
  def self.up
    rename_column :chatrooms, :brand_id, :account_id
    rename_column :themes, :brand_id, :account_id
    rename_table :brands, :accounts
  end 
  
  def self.down
    rename_column :chatrooms, :account_id, :brand_id
    rename_column :themes, :account_id, :brand_id    
    rename_table :accounts, :brands
  end
end
