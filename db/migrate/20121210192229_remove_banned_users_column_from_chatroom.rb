class RemoveBannedUsersColumnFromChatroom < ActiveRecord::Migration
  def self.up
    remove_column :chatrooms, :banned_users
  end

  def self.down
    add_column :chatrooms, :banned_users, :integer
  end
end
