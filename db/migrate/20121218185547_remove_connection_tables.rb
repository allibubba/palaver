class RemoveConnectionTables < ActiveRecord::Migration
  def up
    drop_table :facebook_connections
    drop_table :google_connections
    drop_table :identity_connections
    drop_table :twitter_connections
  end

  def down
  end
end
