class AddChatroomSlugIndex < ActiveRecord::Migration
  def self.up
    add_index :chatrooms, :slug
  end

  def self.down
    remove_index :chatrooms, :slug
  end
end