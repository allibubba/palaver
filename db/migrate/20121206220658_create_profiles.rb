class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :image
      t.integer :user_id
      t.text :description
      t.integer :chat_id
      t.string :display_name

      t.timestamps
    end
  end
end
