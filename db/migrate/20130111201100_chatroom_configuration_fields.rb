class ChatroomConfigurationFields < ActiveRecord::Migration
  def self.up
    add_column :chatrooms, :hide_replies, :boolean
    remove_column :chatrooms, :enable_frontend_moderation
  end

  def self.down
    add_column :chatrooms, :enable_frontend_moderation, :boolean
    remove_column :chatrooms, :hide_replies
  end
end
