class AddNameToChatroom < ActiveRecord::Migration
  def self.up
    add_column :chatrooms, :name, :string
  end

  def self.down
    remove_column :chatrooms, :name
  end
end