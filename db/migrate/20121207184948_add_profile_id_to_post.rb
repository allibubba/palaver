class AddProfileIdToPost < ActiveRecord::Migration
  def self.up
    add_column :posts, :profile_id, :integer
  end

  def self.down
    remove_column :posts, :profile_id
  end
end