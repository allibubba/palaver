class AddProfileIndex < ActiveRecord::Migration
  def up
    add_index :profiles, [:user_id, :chatroom_id], :unique => true
  end

  def down
    remove_index :profiles, [:user_id, :chatroom_id], :unique => true
  end
end
