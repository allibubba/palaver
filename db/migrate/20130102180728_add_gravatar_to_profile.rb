class AddGravatarToProfile < ActiveRecord::Migration
  def self.up
    add_column :profiles, :gravatar, :string
  end

  def self.down
    remove_column :profiles, :gravatar
  end
end