class UpdateColumnInProfile < ActiveRecord::Migration
  def self.up
    rename_column :profiles, :chat_id, :chatroom_id
  end

  def self.down
    rename_column :profiles, :chatroom_id, :chat_id
  end
end