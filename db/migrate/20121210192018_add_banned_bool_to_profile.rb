class AddBannedBoolToProfile < ActiveRecord::Migration
  def self.up
    add_column :profiles, :banned, :boolean
  end

  def self.down
    remove_column :profiles, :banned
  end
end