class AddParentIdToPost < ActiveRecord::Migration
  def self.up
    add_column :posts, :parent_id, :integer
    remove_column :posts, :celebrity_id
    remove_column :posts, :answer_id
    remove_column :posts, :response_id
  end

  def self.down
    add_column :posts, :response_id, :integer
    add_column :posts, :answer_id, :integer
    add_column :posts, :celebrity_id, :integer
    remove_column :posts, :parent_id
  end
end

