class AddStatusableColumns < ActiveRecord::Migration
  def self.up
    add_column :statuses, :statusable_id, :integer
    add_column :statuses, :statusable_type, :string
  end

  def self.down
    remove_column :statuses, :statusable_type
    remove_column :statuses, :statusable_id
  end
end