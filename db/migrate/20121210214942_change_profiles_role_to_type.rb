class ChangeProfilesRoleToType < ActiveRecord::Migration
  def self.up
    rename_column :profiles, :role, :type
  end

  def self.down
    rename_column :profiles, :type, :role
  end
end