class CreateIdentityConnections < ActiveRecord::Migration
  def change
    create_table :identity_connections do |t|

      t.timestamps
    end
  end
end
