class ChangeStatusToString < ActiveRecord::Migration
  def self.up
    change_column :accounts, :status_id, :string
    rename_column :accounts, :status_id, :status

    change_column :chatrooms, :status_id, :string
    rename_column :chatrooms, :status_id, :status

    drop_table :statuses

  end

  def self.down
    rename_column :accounts, :status, :status_id
    change_column :accounts, :status_id, :integer

    rename_column :chatrooms, :status, :status_id
    change_column :chatrooms, :status_id, :integer
  end
end