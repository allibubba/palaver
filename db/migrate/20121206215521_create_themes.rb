class CreateThemes < ActiveRecord::Migration
  def change
    create_table :themes do |t|
      t.integer :brand_id
      t.integer :chatroom_id

      t.timestamps
    end
  end
end
