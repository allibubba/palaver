class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :slug
      t.integer :answer_id
      t.integer :celebrity_id
      t.text :message
      t.string :type

      t.timestamps
    end
  end
end
