class CreateFacebookConnections < ActiveRecord::Migration
  def change
    create_table :facebook_connections do |t|

      t.timestamps
    end
  end
end
