class AddColumnsToConnections < ActiveRecord::Migration
  def change
    add_column :connections, :uid, :string
    add_column :connections, :provider, :string
    add_column :connections, :type, :string
    add_column :connections, :password_digest, :string
    add_column :connections, :nickname, :string
    add_column :connections, :user_id, :integer
    add_column :connections, :token, :string
    add_column :connections, :refresh_token, :string
    add_column :connections, :expires_at , :datetime
  end
end
