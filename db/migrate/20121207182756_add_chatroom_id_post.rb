class AddChatroomIdPost < ActiveRecord::Migration
  def self.up
    add_column :posts, :chatroom_id, :integer
  end

  def self.down
    remove_column :posts, :chatroom_id
  end
end