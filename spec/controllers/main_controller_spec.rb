require 'spec_helper'


describe MainController do

  describe "GET 'index'" do

    it "Guest User arrives at the homepage" do
      @cs = ChatroomStatus.create(:label => 'open')
      20.times {|i|
        Chatroom.create(:chatroom,:status => @cs, :slug => 'slug_#{i}')
      }
      get 'index'
    end

    it "Logged in User arrives at the homepage" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'about'" do
    it "returns http success" do
      get 'about'
      response.should be_success
    end
  end

  describe "GET 'contact'" do
    it "returns http success" do
      get 'contact'
      response.should be_success
    end
  end

end
