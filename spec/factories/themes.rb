# spec/factories/themes.rb

require 'faker'

FactoryGirl.define do

  factory :theme do |f|
    f.account { FactoryGirl.create(:account) }
    f.chatroom { FactoryGirl.create(:chatroom) }
  end

end
