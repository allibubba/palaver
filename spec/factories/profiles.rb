# spec/factories/profiles.rb

require 'faker'

FactoryGirl.define do
  factory :profile do

    image 'profile.gif'
    user_id 1
    description { Faker::Lorem.paragraph(sentence_count = 3) }
    chatroom_id 1
    display_name { Faker::Name.name }
    type 'Viewer'
    banned false
    
  end
end