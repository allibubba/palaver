# spec/factories/chatroom_status.rb

require 'faker'

FactoryGirl.define do
  factory :chatroom_status do |f|
    f.label { Faker::Lorem.word }
    f.type 'ChatroomStatus'
  end
end
