# spec/factories/accounts.rb

require 'faker'

FactoryGirl.define do
  factory :post do |f|
    f.slug { Faker::Lorem.word }
    f.message { Faker::Lorem.paragraph } 
    f.type 'Question'
    f.chatroom { FactoryGirl.create(:chatroom) }
    f.profile { FactoryGirl.create(:profile) }
  end
end
