# spec/factories/users.rb

require 'faker'

FactoryGirl.define do
  factory :user do |f|

    f.email { Faker::Internet.email }
    f.password { Faker::Lorem.word }
    f.name { Faker::Name.name }
    f.slug { Faker::Lorem.word }
    
  end
end