# spec/factories/chatrooms.rb

require 'faker'

FactoryGirl.define do
  factory :chatroom do |f|
    f.slug { Faker::Lorem.word }
    f.fbappid { Faker::Lorem.word }
    f.fbappnamespace { Faker::Lorem.word } 
    f.status_id 1
    f.moderation 1
    f.enable_frontend_moderation 1
    f.theme_id 1
    f.account_id 1
  end
end