# spec/factories/accounts.rb

require 'faker'

FactoryGirl.define do
  factory :account do |f|
    f.slug { Faker::Lorem.word }
    f.name { Faker::Lorem.word } 
    f.fbpage { Faker::Internet.url } 
    f.fbid { Faker::Lorem.word }
  end
end