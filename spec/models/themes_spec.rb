# spec/models/themes_spec.rb
require 'spec_helper'

describe Theme do
  
  it "has a valid Factory" do
    FactoryGirl.create(:theme).should be_valid
  end

  it "is invalid without an account" do
    FactoryGirl.build(:theme, :account => nil).should_not be_valid
  end
  
  it "is invalid without a Chatroom" do
    FactoryGirl.build(:theme, :chatroom => nil).should_not be_valid
  end
end