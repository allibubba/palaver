# spec/models/posts_spec.rb
require 'spec_helper'

describe Post do
  
  it "has a valid Factory" do
    FactoryGirl.create(:post).should be_valid
  end

  it "is invalid without a Profile" do
    FactoryGirl.build(:post, :profile => nil).should_not be_valid
  end
  
  it "is invalid without a Chatroom" do
    FactoryGirl.build(:post, :chatroom => nil).should_not be_valid
  end
end