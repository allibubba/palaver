# spec/models/profiles_spec.rb
require 'spec_helper'

describe Profile do
  
  it "has a valid Factory" do
    FactoryGirl.create(:profile).should be_valid
  end

  it "is invalid without a User" do
    FactoryGirl.build(:profile, :user => nil).should_not be_valid
  end

  it "is invalid without a Chatroom" do
    FactoryGirl.build(:profile, :chatroom => nil).should_not be_valid
  end

  it "is invalid without a Type" do
    FactoryGirl.build(:profile, :type => nil).should_not be_valid
  end
  
  it "has unique combination of user and chatroom" do
    prof = FactoryGirl.create(:profile, :user_id => 1, :chatroom_id =>2)
    FactoryGirl.build(:profile, :user_id => prof.user_id, :chatroom_id =>prof.chatroom_id)
    FactoryGirl.build(:profile, :user_id => prof.user_id, :chatroom_id =>prof.chatroom_id).should_not be_valid
  end


end