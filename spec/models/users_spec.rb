# spec/models/users_spec.rb
require 'spec_helper'

describe User do
  
  it "has a valid Factory" do
    FactoryGirl.create(:user).should be_valid
  end
  
  it "is invalid without an Email" do
    FactoryGirl.build(:user, :email => nil).should_not be_valid
  end

  it "is invalid without a Password" do
    FactoryGirl.build(:user, :password => nil).should_not be_valid
  end

  it "is invalid without a Name" do
    FactoryGirl.build(:user, :name => nil).should_not be_valid
  end

  it "is invalid without a unique Email" do
    usr = FactoryGirl.create(:user)
    FactoryGirl.build(:user, :email => usr.email)
    FactoryGirl.build(:user, :email => usr.email).should_not be_valid
  end

  it "is invalid without a unique Slug" do
    usr = FactoryGirl.create(:user)
    FactoryGirl.build(:user, :slug => usr.slug)
    FactoryGirl.build(:user, :slug => usr.slug).should_not be_valid
  end

end