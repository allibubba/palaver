# spec/models/chatrooms_spec.rb
require 'spec_helper'

describe Chatroom do
  
  it "has a valid Factory" do
    FactoryGirl.create(:chatroom).should be_valid
  end

  it "is invalid without a unique Slug" do
    cr = FactoryGirl.create(:chatroom)
    FactoryGirl.build(:chatroom, :slug => cr.slug)
    FactoryGirl.build(:chatroom, :slug => cr.slug).should_not be_valid
  end  

end