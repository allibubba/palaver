# spec/models/accounts_spec.rb
require 'spec_helper'

describe Account do
  
  it "has a valid Factory" do
    FactoryGirl.create(:account).should be_valid
  end

  it "is invalid without a unique Slug" do
    ac = FactoryGirl.create(:account)
    FactoryGirl.build(:account, :slug => ac.slug)
    FactoryGirl.build(:account, :slug => ac.slug).should_not be_valid
  end  

  it "is invalid without fbid" do
    FactoryGirl.build(:account, :fbid => nil).should_not be_valid
  end  

  it "is invalid without fbpage" do
    FactoryGirl.build(:account, :fbpage => nil).should_not be_valid
  end  
end